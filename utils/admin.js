const admin = require("firebase-admin");
const functions = require("firebase-functions/v1");
const { Storage } = require("@google-cloud/storage");

const db = admin.firestore();
const FieldValue = admin.firestore.FieldValue;
const Timestamp = admin.firestore.Timestamp;
const storage = new Storage();

module.exports = {
  admin,
  db,
  FieldValue,
  Timestamp,
  storage,
  functions,
};