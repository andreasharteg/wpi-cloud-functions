"use strict";

const { db, functions, FieldValue } = require("../utils/admin");

exports.addDeviceLocation = async (req, res) => {

  const deviceId = req.params.deviceId;

  const body = req.body;

  console.log(`addDeviceLocation - body: ${JSON.stringify(body)}`);


  try {
    await addDeviceLog(deviceId, body);
  } catch (e) {
    console.error('error saving to device log... update source code to see more details');
  }

  const data = {
    ...body,
    deviceId: deviceId,
    lastLocationAt: FieldValue.serverTimestamp(),
  }

  await db
    .collection(`devices`)
    .doc(deviceId)
    .set(data, { merge: true, ignoreUndefinedProperties: true });

  return res.status(201).json({
    status: "ok",
    data: {
      command: "todo - command if put in queue or triggered by location",
    }
  });
}

exports.addDeviceLogMessage = async (req, res) => {

  const deviceId = req.params.deviceId;

  const body = req.body;

  const writeResult = await addDeviceLog(deviceId, body);

  res.json({ result: `device (deviceId: ${deviceId}) added new log message with ID: ${writeResult.id}` });
}

async function addDeviceLog(deviceId, message) {
  const data = {
    ...message,
    loggedAt: FieldValue.serverTimestamp(),
  }
  const writeResult = await db
    .collection(`devices/${deviceId}/log_messages`)
    .add(data);
  return writeResult;
}

// Firestore triggers: https://firebase.google.com/docs/functions/firestore-events?gen=1st
// Can trigger cloud functions when data is uploaded to firestore
//**  WARNING never make trigger that triggers it self !!! *// 
//**  to avoid it calling it self in a loop until budget runs out *//
exports.onDeviceMessageModified = functions.region('europe-west1').firestore
  .document("/devices/{deviceId}/message")
  .onUpdate(async (change, context) => {
    const action = !change.after.exists ? 'deleted' : !change.before.exists ? 'created' : 'updated';

    const deviceId = context.params.deviceId;

    if (action === 'created' || action === 'updated') { 
      
      const data = change.after.data();           
      
      await db
        .collection(`devices`)
        .doc(deviceId)
        .set({deviceId: deviceId, lastMessage: data}, { merge: true });

    } else if (action == 'deleted') {
      const data = change.before.data();

    }
});