"use strict";

// The Firebase Admin SDK to access Firestore.
const admin = require("firebase-admin");
const functions = require('firebase-functions/v1');
const express = require('express');
const app = express();

admin.initializeApp();

const { addDeviceLogMessage, addDeviceLocation } = require("./api/devices");

// define routes: (https://expressjs.com/en/guide/routing.html)
app.get("/", (req, res) => res.status(200).send("Hello GoBoat!"));
app.post("/devices/:deviceId/messages", addDeviceLogMessage);
app.post("/devices/:deviceId/locations", addDeviceLocation);
// todo - make more..

exports.api = functions.region('europe-west1').https.onRequest(app);

